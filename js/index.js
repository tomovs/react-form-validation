const { Component, PropTypes } = React;

class Form extends Component {
  constructor(props){
    super(props);
    this.fields = [
      {name:'name', type:'input', subtype:'text', label:"First name, Last name", icon:"user", validation:'name', validMessage:'success', required:true, valid:false},
      {name:'username', type:'input', subtype:'text', label:"Username", icon:"user-secret", validation:'username', required:true, valid:false},
      {name:'email', type:'input', subtype:'email', icon:"envelope", validation:'email', label:"Email", required:true, valid:false},
      {name:'password', type:'input', subtype:'password', label:"Password", icon:"lock", validation:'password', required:true, valid:false},
      {name:'date', type:'input', subtype:'date', icon:"calendar", label:"Date of birth", required:true, valid:false},
      {name:'phone', type:'input', subtype:'number', label:"Phone number", icon:"phone", validation:'phone', required:true, valid:false},
      {name:'radio', type:'radio', subtype:'radio', label:"Gender: 1.Male 2.Female", icon:"venus-mars", validation:'radio', required:false, valid:true},
      {name:'select', type:'select', subtype:'select', label:"Country", icon:"flag", required:true, valid:false},
      {name:'checkbox', type:'checkbox', subtype:'checkbox', label:"Registration agreement", icon:"check", required:true, valid:true}
    ]
    this.validation={
      /* регулярні вираження для валідації полів. */ 
      email:/[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*/,
      name:/[a-zA-Z0-9!#$%&'*+/=?^_`{|}~.-]{4,}/,
      username:/^[0-9a-zA-Z\_]+$/,
      password:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[A-Za-z0-9]{6,}$/,
      phone:/^\d{10}$/, 
    }
    this.state={};
    this.fields.map(field=>{
      this.state[field.name] = {content:false, valid:false}
    });
  }
  updateField(field, value, valid){
    var obj = {};
    obj[field] = {content:value, valid:valid};
    this.setState(obj);
  }
  validate(e){
    e.preventDefault();
    for(let i in this.state){
      if(!this.state[i].valid) return false;
    }
    console.log('all clear')
  }
  renderFields(){
    return this.fields.map(field=>{
      return <Field type={field.type} subtype={field.subtype} name={field.name} label={field.label} required={field.required} key={field.name} updateFunc={this.updateField.bind(this)} validation={this.validation[field.validation]} validMessage={field.validMessage} icon={field.icon} />
    })
  }
  render(){
    return(
      <form className="form" onSubmit={(e)=>{this.validate(e)}} noValidate>
        {this.renderFields()}
        <div className="container">
          <input className="" type="submit" value="Register" />
        </div>
      </form>
    )
  }
}

class Field extends Component {
  constructor(props){
    super(props);
    this.hasFocus = false;
    this.state={value: "", dirty:false, valid:false}
  }
  updateField(e){
    var valid = this.validate(e.target.value)
    this.setState({value: e.target.value, dirty:true, valid:valid});
    this.props.updateFunc(this.props.name, e.target.value, valid)
  }
  handleFocus(){
    this.setState({hasFocus:true});
  }
  handleBlur(){
    this.setState({hasFocus:false});
  }
  handleLabelClick(e){
    e.preventDefault();
    if(this.state.hasFocus) return false;
    this.refs.input.focus();
    this.setState({hasFocus: true})
  }
  validate(str){
    return str.match(this.props.validation);
  }
  render(){
    // аддони для компонента
    const focusClass = this.state.value.length || this.state.hasFocus ? 'active' : "";
    const validClass = !this.state.dirty ? "" : this.state.valid ? "valid" : "invalid";
    const validMessage = this.props.validMessage ? <span className="valid-message">({this.props.validMessage})</span> : "";
    const label = this.props.label ? 
              <label htmlFor={this.props.name} onClick={(e)=>{this.handleLabelClick(e)}}>{this.props.label} {validMessage}</label> : "";
    const icon = !this.props.icon ? "" : this.state.valid ? <i className={`fa fa-thumbs-up`} /> : <i className={`fa fa-${this.props.icon}`} />;
    
    // компонент - типи
    switch(this.props.type){
      case 'input':
        return(
          <div className={`form-group ${focusClass}`}>
            <input autoComplete='off' ref="input" className={`form-control  ${validClass}`} name={this.props.name} id={this.props.name} type={this.props.subtype} required={this.props.required} value={this.state.value} onChange={(e)=>{this.updateField(e)}} onFocus={(e)=>{this.handleFocus()}} onBlur={(e)=>{this.handleBlur()}} />
            {icon}
            {label}
          </div>
        )
      case 'textarea':
        return (
          <div className={`form-group ${focusClass}`}>
            <textarea ref="input" className={`form-control  ${validClass}`} name={this.props.name} id={this.props.name} required={this.props.required} onChange={(e)=>{this.updateField(e)}} onFocus={(e)=>{this.handleFocus()}} onBlur={(e)=>{this.handleBlur()}}>{this.state.value}</textarea>
            {icon}
            {label}
          </div>
        )
      case 'checkbox':
        return (
          <div className={`form-group ${focusClass}`}>
            <input ref="input" className={`form-control  ${validClass}`} name={this.props.name} id={this.props.name} type={this.props.subtype} required='required' value={this.state.value} onChange={(e)=>{this.updateField(e)}} onFocus={(e)=>{this.handleFocus()}} onBlur={(e)=>{this.handleBlur()}} />
            {icon}
            {label}
          </div>
        )
      case 'radio':
        return (
          <div className={`form-group ${focusClass}`}>
            <input ref="input" className={`form-control  ${validClass}`} id={this.props.htmlFor} name='radio' id={this.props.name} type={this.props.subtype} required={this.props.required} onChange={(e)=>{this.updateField(e)}} onFocus={(e)=>{this.handleFocus()}} onBlur={(e)=>{this.handleBlur()}} />
            <input ref="input" className={`form-control  ${validClass}`} id={this.props.htmlFor} name='radio' id={this.props.name} type={this.props.subtype} required={this.props.required} onChange={(e)=>{this.updateField(e)}} onFocus={(e)=>{this.handleFocus()}} onBlur={(e)=>{this.handleBlur()}} />
            {icon}
            {label}
          </div>
        )
      case 'select':
        return (
          <div className={`form-group ${focusClass}`}>
            <select ref="input" className={`form-control  ${validClass}`} name={this.props.name} value={this.state.value} id={this.props.name} type={this.props.subtype} required={this.props.required} onChange={(e)=>{this.updateField(e)}} onFocus={(e)=>{this.handleFocus()}} onBlur={(e)=>{this.handleBlur()}}>
            <option value='' disabled>Select one option</option>
            <option value='USA'>USA</option>
            <option value='France'>France</option>
            <option value='Italy'>Italy</option>
            <option value='England'>England</option>
            <option value='Germany'>Germany</option>
            <option value='Ukraine'>Ukraine</option>
            <option value='Other country'>Other country</option>
            </select>
            {icon}
            {label}
          </div>
        )
      default:
        return <div>incomplete field</div>
    }  
  }
}

ReactDOM.render(<Form />, document.getElementById('reactForm'));